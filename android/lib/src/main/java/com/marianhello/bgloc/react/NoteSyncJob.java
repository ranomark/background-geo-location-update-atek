package com.marianhello.bgloc.react;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.marianhello.bgloc.BackgroundGeolocationFacade;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class NoteSyncJob extends Job {
    public static final String TAG = "job_note_sync";
    private BackgroundGeolocationFacade facade;

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        final Context context = getContext();
        int responseCode = 0;

        try {
            responseCode = postHeartbeatEvent(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("@JobScheduler from atek", String.valueOf(responseCode));
        return Result.SUCCESS;
    }


    public static int postHeartbeatEvent(Context context) throws IOException {
        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA",
                Context.MODE_PRIVATE);
        final String domain = sharedPreferences.getString("DOMAIN", null);
        final String token = sharedPreferences.getString("TOKEN", null);

        Map httpHeaders = new HashMap();
        httpHeaders.put("Authorization"," app "+token);

        HttpURLConnection conn = (HttpURLConnection) new URL(domain + "/devices/heartbeat").openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        Iterator<Map.Entry<String, String>> it = httpHeaders.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            conn.setRequestProperty(pair.getKey(), pair.getValue());
        }

        Log.d("@JobScheduler from atek",domain + "/devices/heartbeat" +  httpHeaders);
        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
        }catch (Exception e){
            Log.d("ERRORMESSAGE SENDDATA", String.valueOf(e));

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        Log.d("HEARTBEAT",conn.getResponseMessage());
        return conn.getResponseCode();
    }

    public static void scheduleJob() {
        Set<JobRequest> jobRequests = JobManager.instance().getAllJobRequestsForTag(NoteSyncJob.TAG);
        if (!jobRequests.isEmpty()) {
            return;
        }
        new JobRequest.Builder(NoteSyncJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setUpdateCurrent(true) // calls cancelAllForTag(NoteSyncJob.TAG) for you
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
    }
}
