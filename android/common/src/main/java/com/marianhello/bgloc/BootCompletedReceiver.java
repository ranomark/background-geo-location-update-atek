/*
According to apache license

This is fork of christocracy cordova-plugin-background-geolocation plugin
https://github.com/christocracy/cordova-plugin-background-geolocation

This is a new class
*/

package com.marianhello.bgloc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.marianhello.bgloc.data.ConfigurationDAO;
import com.marianhello.bgloc.data.DAOFactory;

import org.json.JSONException;
/**
 * BootCompletedReceiver class
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    private static final String TAG = BootCompletedReceiver.class.getName();

    @Override
     public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received boot completed");
        ConfigurationDAO dao = DAOFactory.createConfigurationDAO(context);
        Config config = null;

        try {
            config = dao.retrieveConfiguration();
        } catch (JSONException e) {
            //noop
        }

        if (config == null) { return; }

        Log.d(TAG, "Boot completed " + config.toString());

        Log.d(TAG, "Starting service after boot");

        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA",
                Context.MODE_PRIVATE);
         String startService = sharedPreferences.getString("STARTSERVICE", null);

        Log.d(TAG, "startService" + startService);

        if(startService.equals("YES")){
            Intent locationServiceIntent = new Intent(context, LocationService.class);
            locationServiceIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
            locationServiceIntent.putExtra("config", config);

            Log.d(TAG, "SDK int" + String.valueOf(Build.VERSION.SDK_INT));

            if (Build.VERSION.SDK_INT >=  26) {
                Log.d(TAG, "Starting service after boot on OREO");
                ContextCompat.startForegroundService(context, locationServiceIntent);
            }else{
                context.startService(locationServiceIntent);
            }
        }


//
//        if (config.getStartOnBoot()) {
//            Log.i(TAG, "Starting service after boot");
//            Intent locationServiceIntent = new Intent(context, LocationService.class);
//            locationServiceIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
//            locationServiceIntent.putExtra("config", config);
//
//            context.startService(locationServiceIntent);
//        }
     }
}
