package com.marianhello.bgloc;

import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class HttpPostService {

    public static int postJSON(String url, Object json, Map headers) throws IOException {
        String jsonString = json.toString();
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setDoOutput(true);
        conn.setFixedLengthStreamingMode(jsonString.length());
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            conn.setRequestProperty(pair.getKey(), pair.getValue());
        }

        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
            os.write(json.toString());
        }catch (Exception e){
            Log.d("ERRORMESSAGE SENDDATA", String.valueOf(e));

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        Log.d("ERRORMESSAGE",conn.getResponseMessage() + json.toString() + url + headers.toString());
        return conn.getResponseCode();
    }


    public static int postFile(String url, File file, Map headers, UploadingCallback callback) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

        conn.setDoInput(false);
        conn.setDoOutput(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            conn.setFixedLengthStreamingMode(file.length());
        } else {
            conn.setChunkedStreamingMode(0);
        }
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            conn.setRequestProperty(pair.getKey(), pair.getValue());
        }


        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            Log.d("FILEUPLOAD error",String.valueOf(e));
        }


        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
            os.write(text.toString());
        }catch (Exception e){

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        Log.d("ERRORMESSAGEFROMFILE",conn.getResponseMessage() + text.toString() + url + headers.toString());
        return conn.getResponseCode();



    }
}
